def calculator(a, b, operator):
    # ==============
    # Your code here
    import math
    if operator == "+":
        return bin(a + b).replace("0b", "")
    elif operator == "-":
        return bin(a - b).replace("0b", "")
    elif operator == "*":
        return bin(a * b).replace("0b", "")
    else:
        num = math.floor(a / b)
        return bin(int(num)).replace("0b", "")
    # ==============

print(calculator(2, 4, "+")) # Should print 110 to the console
print(calculator(10, 3, "-")) # Should print 111 to the console
print(calculator(4, 7, "*")) # Should output 11100 to the console
print(calculator(100, 2, "/")) # Should print 110010 to the console
